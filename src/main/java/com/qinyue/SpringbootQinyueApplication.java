package com.qinyue;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootQinyueApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringbootQinyueApplication.class, args);
	}

}
